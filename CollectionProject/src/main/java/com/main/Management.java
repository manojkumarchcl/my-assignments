package com.main;

import java.util.ArrayList;
import java.util.List;

import com.exception.EmployeeException;
import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

/**
 * @author manoj.kumarc
 *
 */
public class Management {

	public static void main(String[] args) {
		
		Employee employee1 = new Employee(3107, "Potter", 23000.00f);
		Employee employee2 = new Employee(1010, "Nate", 29000.00f);
		Employee employee3 = new Employee(2709, "Mark", 50000.00f);
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		
		EmployeeService employeeService = new EmployeeServiceImpl();
			try {
				Employee employee = employeeService.searchEmployeeById(employees, 1010);
				System.out.println("Employeee details are: " +"\n");
				if(employee != null) {
					System.out.println("Employee Id is: " +employee.getEmployeeId());
					System.out.println("Employee Name is: " +employee.getEmployeeName());
					System.out.println("Employee Salary is: "+employee.getEmployeeSalary());	
				}
				else {
					System.out.println("Not Available");
				}
			}catch(EmployeeException e) {
				System.err.println(e.getMessage());
			}
			System.out.println("\n");
			try {
				Employee employee = employeeService.searchEmployeeByName(employees, "Mark");
				System.out.println("Employeee details are: " +"\n");
				if(employee != null) {
					System.out.println("Employee Id is: " +employee.getEmployeeId());
					System.out.println("Employee Name is: " +employee.getEmployeeName());
					System.out.println("Employee Salary is: "+employee.getEmployeeSalary());	
				}
				else {
					System.out.println("Not Available");
				}
			}catch(EmployeeException e) {
				System.err.println(e.getMessage());
			}
			
	}
}
