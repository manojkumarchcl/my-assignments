package com.service;

import java.util.List;
import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public interface EmployeeService {

	Employee searchEmployeeByName(List<Employee> list, String eName) throws EmployeeException;
	Employee searchEmployeeById(List<Employee> list, int id) throws EmployeeException;
	Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException;
	Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException;
}
