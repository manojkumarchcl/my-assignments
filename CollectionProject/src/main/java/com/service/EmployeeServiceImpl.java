package com.service;

import java.util.List;
import java.util.Set;

import com.exception.EmployeeException;
import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public Employee searchEmployeeByName(List<Employee> list, String eName) throws EmployeeException {
	Employee emp = null;
for (Employee employee : list) {
	if(employee.getEmployeeName().equals(eName)) {
		emp = employee;
	}
	else {
		throw new EmployeeException("Employee not exists: "+eName);
	}
}
		return emp;
	}

	@Override
	public Employee searchEmployeeById(List<Employee> list, int id) throws EmployeeException {
		Employee emp = null;
		for (Employee employee : list) {
			if(employee.getEmployeeId() == id) {
				emp = employee;
			}
		}return emp;

	}

	@Override
	public Employee searchEmployeeByName(Set<Employee> set, String eName) throws EmployeeException {
		Employee emp = null;
		for (Employee employee : set) {
			if(employee.getEmployeeName().equals(eName)) {
				emp = employee;
			}
			else {
				throw new EmployeeException("Employee not exists: "+eName);
			}
		}
				return emp;
	}

	@Override
	public Employee searchEmployeeById(Set<Employee> set, int id) throws EmployeeException {
		Employee emp = null;
		for (Employee employee : set) {
			if(employee.getEmployeeId() == id) {
				emp = employee;
			}
		}return emp;
	}
}
