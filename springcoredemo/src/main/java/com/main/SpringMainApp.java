package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public class SpringMainApp {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/hello.xml");
		Employee employee = (Employee) applicationContext.getBean("employeeId");
		System.out.println("Spring" + employee);
		System.out.println("Employee Number: " + employee.getEmpNo());
		System.out.println("Employee Name: " + employee.getEmpName());

		Employee employee2 = new Employee();
		System.out.println("Java:" + employee2);
		employee2.setEmpNo(6877);
		employee2.setEmpName("Vasu");
		System.out.println("Employee Number: " + employee2.getEmpNo());
		System.out.println("Employee Name: " + employee2.getEmpName());
	}

}
