package com.model;
/**
 * @author manoj.kumarc
 *
 */
public class Employee {
private int empId;
private String empName;
private float empSalary;
private Department departments;
public Employee() {
	super();
}
public Employee(int empId, String empName, float empSalary) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.empSalary = empSalary;
}

public Employee(Department departments) {
	super();
	this.departments = departments;
}
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public float getEmpSalary() {
	return empSalary;
}
public void setEmpSalary(float empSalary) {
	this.empSalary = empSalary;
}
public Department getDepartment() {
	return departments;
}
public void setDepartment(Department departments) {
	this.departments = departments;
}
}
