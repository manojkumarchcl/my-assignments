package com.model;

import java.util.List;
import java.util.Set;

/**
 * @author manoj.kumarc
 *
 */
public class Department{
private int deptId;
private String deptName;

private List<Employee> employees;

public Department() {
	super();
	// TODO Auto-generated constructor stub
}

public Department(List<Employee> employees) {
	super();
	this.employees = employees;
}

public List<Employee> getEmployees() {
	return employees;
}

public void setEmployees(List<Employee> employees) {
	this.employees = employees;
}

public Department(int deptId, String deptName) {
	super();
	this.deptId = deptId;
	this.deptName = deptName;
}

public int getDeptId() {
	return deptId;
}

public void setDeptId(int deptId) {
	this.deptId = deptId;
}

public String getDeptName() {
	return deptName;
}

public void setDeptName(String deptName) {
	this.deptName = deptName;
}

}
