package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Department;
import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public class SpringMain {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/injection.xml");
        Employee employee = (Employee) applicationContext.getBean("employee");
        System.out.println("-------------Employee Constructor Injection---------------");
        System.out.println("Employee Number is: " +employee.getEmpId());
        System.out.println("Employee Name is: " +employee.getEmpName());
        System.out.println("Employee Salary is: " +employee.getEmpSalary());
        
        Employee employee2 = (Employee) applicationContext.getBean("employee1");
        System.out.println("-------------Employee Setter Injection---------------");
        System.out.println("Employee Number is: " +employee2.getEmpId());
        System.out.println("Employee Name is: " +employee2.getEmpName());
        System.out.println("Employee Salary is: " +employee2.getEmpSalary());
        
        Department department = (Department) applicationContext.getBean("department");
        System.out.println("-------------Department Setter Injection---------------");
        System.out.println("Department Id is: " +department.getDeptId());
        System.out.println("Department Name is: " +department.getDeptName());
        
        Department department1 = (Department) applicationContext.getBean("department1");
        System.out.println("-------------Department Constructor Injection---------------");
        System.out.println("Department Id is: " +department1.getDeptId());
        System.out.println("Department Name is: " +department1.getDeptName());
        
       
	}

}
