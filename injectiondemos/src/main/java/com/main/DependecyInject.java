package com.main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Department;
import com.model.Employee;

public class DependecyInject {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/config/injection.xml");
		Employee employee3 = (Employee) applicationContext.getBean("employee1");
		System.out.println("-------------Dependency Injection---------------");
		System.out.println("Employee Number is: " + employee3.getEmpId());
		System.out.println("Employee Name is: " + employee3.getEmpName());
		System.out.println("Employee Salary is: " + employee3.getEmpSalary());

		
		Department department = (Department) applicationContext.getBean("department");
		List<Employee> employee = department.getEmployees();
		for (Employee employees2 : employee) {
		System.out.println("--------Dependency Injection 2---------");
		System.out.println("Employees in Java Development");
			System.out.println("Employee Name: " +employees2.getEmpName());
			System.out.println("Employee Id: " +employees2.getEmpId());
			System.out.println("Employee Salary: " +employees2.getEmpSalary());
		}
	
		Employee employeeRole = (Employee) applicationContext.getBean("employee4");
		System.out.println("-------------From Properties File ---------------");
		System.out.println("Employee Number is: " +employeeRole.getEmpId());
		System.out.println("Employee Name is: " +employeeRole.getEmpName());
		System.out.println("Employee Salary is: " +employeeRole.getEmpSalary());
	}

}
