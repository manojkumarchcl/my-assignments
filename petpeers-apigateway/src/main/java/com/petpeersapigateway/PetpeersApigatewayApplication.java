package com.petpeersapigateway;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
/**
 * @author manoj.kumarc
 *
 */
@EnableEurekaClient
@SpringBootApplication
public class PetpeersApigatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetpeersApigatewayApplication.class, args);
	}

}
