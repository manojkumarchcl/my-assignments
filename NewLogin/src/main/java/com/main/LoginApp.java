package com.main;

import java.util.Scanner;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

/**
 * @author manoj.kumarc
 *
 */
public class LoginApp {

	public static void main(String[] args) {
		System.out.println("Please Login: "+"\n");
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the UserId: ");
		int id = scanner.nextInt();
		
		System.out.println("Enter the password: ");
		String pwd = scanner.next();
		System.out.println("\n");
		UserService userService = new UserServiceImpl();
		User user = userService.validateUserIdAndPassword(id,pwd);
		if(user != null) {
			System.out.println("Login Successfull, Welcome to HCL " +user.getUserName());
			}
		else {
			System.err.println("Invalid Username and password.");
		}
	}

}
