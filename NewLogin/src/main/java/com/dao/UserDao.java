package com.dao;

import com.model.User;

/**
 * @author manoj.kumarc
 *
 */
public interface UserDao {

	public abstract User createUser(User user);
	public abstract User readUserById(int userId);
	public abstract User updateUser(User user);
	public abstract int deleteUserByUserId(int empId);
	public abstract User validateUserIdAndPassword(int userId, String password);
	
}
