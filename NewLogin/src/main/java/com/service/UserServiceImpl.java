package com.service;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;

/**
 * @author manoj.kumarc
 *
 */
public class UserServiceImpl implements UserService {

	@Override
	public User createUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User readUserById(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User updateUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteUserByUserId(int empId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public User validateUserIdAndPassword(int userId, String password) {
		User user = null;
		int length = String.valueOf(userId).length();
		if(length > 5 && password.length() > 5) {
			UserDao userDao = new UserDaoImpl();
			user = userDao.validateUserIdAndPassword(userId, password);
		}else {
			user = null;
		}
		
		return user;
	}

}
