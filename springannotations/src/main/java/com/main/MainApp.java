package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

/**
 * @author manoj.kumarc
 *
 */
public class MainApp {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(Demo1.class);
        Student student = applicationContext.getBean(Student.class);
        System.out.println(student.getStudentNo());
        System.out.println(student.getStudentName());
        
        ApplicationContext applicationContext2 = new ClassPathXmlApplicationContext("com/config/springconfig.xml");
        Student student2 = (Student) applicationContext2.getBean("student");
        System.out.println("Spring with XML and Annotation +Service with Autowiring & Constructor injection");
        System.out.println(student.getStudentNo());
        System.out.println(student.getStudentName());
        System.out.println(student.getMarks());
        System.out.println(student.getAddress().getDoorNo());
        System.out.println(student.getAddress().getCity());
        System.out.println(student.getAddress().getState());
        
        
	}

}
