package com.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author manoj.kumarc
 *
 */
//@Component
@Service
public class Address {
	@Value(value = "27")
	private int doorNo;
	@Value(value = "Chennai")
	private String City;
	@Value(value = "TamilNadu")
	private String State;

	public Address() {
		super();
	}

	public Address(int doorNo, String city, String state) {
		super();
		this.doorNo = doorNo;
		City = city;
		State = state;
	}

	public int getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(int doorNo) {
		this.doorNo = doorNo;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

}
