package com.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;


/**
 * @author manoj.kumarc
 *
 */
//@Component
@Service
@PropertySource("classpath:com/config/root.properties")
public class Student {
//	@Value(value = "999")
	private int studentNo;
//	@Value(value = "Manoj")
	private String studentName;
//	@Value(value = "1072")
	private int marks;
	@Autowired
	private Address address;//autowired by type
	
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Student() {
		super();
	}
	@Autowired
	public Student(@Value ("${abc}") int studentNo, @Value ("${def}") String studentName, @Value ("${ghi}") int marks) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.marks = marks;
	}
	public int getStudentNo() {
		return studentNo;
	}
	
	public void setStudentNo(int studentNo) {
		this.studentNo = studentNo;
	}
	public String getStudentName() {
		return studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public int getMarks() {
		return marks;
	}
	@Value(value = "1072")
	public void setMarks(int marks) {
		this.marks = marks;
	}
	
}
