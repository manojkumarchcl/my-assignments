package com.hcl.petproducer.exception;

public class PetProducerException extends Exception {
	
	private String customMessage;

	public PetProducerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PetProducerException(String customMessage) {
		super();
		this.customMessage = customMessage;
	}

	public String getCustomMessage() {
		return customMessage;
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}


	
	
}
