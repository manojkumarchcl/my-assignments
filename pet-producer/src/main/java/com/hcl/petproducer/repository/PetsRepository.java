package com.hcl.petproducer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.petproducer.model.Pet;
/**
 * @author manoj.kumarc
 *
 */
@Repository
public interface PetsRepository extends JpaRepository<Pet, Long> {

}
