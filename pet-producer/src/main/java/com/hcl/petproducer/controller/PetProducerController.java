package com.hcl.petproducer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.petproducer.exception.PetProducerException;
import com.hcl.petproducer.model.Pet;
import com.hcl.petproducer.service.ConsumerClient;
import com.hcl.petproducer.service.PetProducerService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author manoj.kumarc
 *
 */
@RestController
@Slf4j
@RequestMapping(value = "petproducer")
public class PetProducerController {
	@Autowired
	private ConsumerClient consumerClient; 
	@Autowired
	private PetProducerService petProducerService;
	@PostMapping(value = "createPets")
	public ResponseEntity<Pet> createPets(@RequestBody Pet pet){
		log.info("Created Pet Id is:" +pet.getPetId());
		log.info("Created Pet Name is:" +pet.getPetName());
		log.info("Created Pet Age is:" +pet.getPetAge());
		log.info("Created Pet Place is:" +pet.getPetPlace());
		Pet pets = null;
		if (pet != null) {
			pets = petProducerService.createPet(pet);
		}
		return new ResponseEntity<Pet>(pet, HttpStatus.CREATED);
	}
	@GetMapping(value = "readpets")
	public List<Pet> readPets(){
		return petProducerService.readAllPet();	
	}

	@PutMapping(value = "/updatepet")
	public Pet alterPet(@RequestBody Pet pet) {
		log.info("Updated Pet Id is:" +pet.getPetId());
		log.info("Updated Pet Name is:" +pet.getPetName());
		log.info("Updated Pet Age is:" +pet.getPetAge());
		log.info("Updated Pet Place is:" +pet.getPetPlace());
		
		return petProducerService.updatePet(pet);
	}

	@GetMapping(value = "readbypetid/{petId}")
	public Pet readPetByPetId(@PathVariable Long petId) throws PetProducerException {
		log.info("Read Pet Id is:" +petId);
		Pet pet = null;
		if (petId > 0) {
			pet = petProducerService.readPetById(petId);
			
		}
	
		return pet;
	}
	@DeleteMapping(value = "deletebypetid/{petId}")
	public Long deletepet(@PathVariable Long petId) {
		log.info("Deleted Pet Id is:" +petId);
		return petProducerService.deletePetById(petId);
	}
	@PutMapping("/buy/pet")
	public ResponseEntity<Object> buyPet(@Valid @RequestBody Pet petRequest) throws PetProducerException {
		Pet pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = petProducerService.savePetWithUser(petRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.CONFLICT);
			}
		} catch (PetProducerException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
}
