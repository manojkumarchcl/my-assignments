package com.hcl.petproducer.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.petproducer.exception.PetProducerException;
import com.hcl.petproducer.model.Pet;
import com.hcl.petproducer.repository.PetsRepository;
/**
 * @author manoj.kumarc
 *
 */
@Service
public class PetProducerServiceImpl implements PetProducerService {
	@Autowired
	private PetsRepository petsRepository;

	@Override
	@Transactional
	public Pet createPet(Pet pet) {
		return petsRepository.save(pet);
	}

	@Override
	@Transactional
	public Pet updatePet(Pet pet) {
		return petsRepository.save(pet);
	}

	@Override
	@Transactional
	public List<Pet> readAllPet() {
		return petsRepository.findAll();
	}

	@Override
	@Transactional
	public Pet readPetById(Long petId) throws PetProducerException {
		Pet pet = null;
		Optional<Pet> optionalPet = petsRepository.findById(petId);
		if (optionalPet.isPresent()) {
			pet = optionalPet.get();
		}else {
			throw new PetProducerException("Pet not exists with given pet id");
		}
		return pet;
	}

	@Override
	@Transactional
	public Long deletePetById(Long id) {
		petsRepository.deleteById(id);
		return id;
	}

	@Override
	public Pet savePetWithUser(Pet petExists) throws PetProducerException {
		Pet petCreated = null;
		ModelMapper modelMapper = new ModelMapper();
		Pet petRequested = modelMapper.map(petExists, Pet.class);
		petCreated = petsRepository.save(petRequested);
		if (petCreated != null) {
			return petCreated;
		} else {
			throw new PetProducerException("Pet Not created");
		}
	}

	
}
