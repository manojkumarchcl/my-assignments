package com.hcl.petproducer.service;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.hcl.petproducer.model.UserDto;

/**
 * @author manoj.kumarc
 *
 */
@FeignClient(name = "consumer/usersconsumers")
public interface ConsumerClient {

	@PostMapping("/adduser")
	public abstract ResponseEntity<UserDto> addUser(@RequestBody @Valid UserDto user);

	@GetMapping("/login")
	public abstract ResponseEntity<Object> loginUser(@RequestParam("name") String name,
			@RequestParam("password") String password);

	@GetMapping("/logout")
	public abstract ResponseEntity<String> logout();
}
