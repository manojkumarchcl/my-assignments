package com.hcl.petproducer.service;

import java.util.List;

import com.hcl.petproducer.exception.PetProducerException;
import com.hcl.petproducer.model.Pet;


/**
 * @author manoj.kumarc
 *
 */
public interface PetProducerService {
	public abstract Pet createPet(Pet pet);

	public abstract Pet readPetById(Long petId) throws PetProducerException;

	public abstract Pet updatePet(Pet pet);

	public abstract Long deletePetById(Long id);

	public abstract List<Pet> readAllPet();
	
	public Pet savePetWithUser(Pet petExists) throws PetProducerException;
}
