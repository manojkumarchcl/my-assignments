package com.hcl.petproducer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name= "PETS")
public class Pet implements Serializable {
	
	private static final long serialVersionUID = 4316251877901475542L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PETS_ID")
	private Long petId;
	@NotNull
	@Size(min=3,max=12,message = "Petname should be in 3 to 12 Characters")
	@Column(name = "PET_NAME")
	private String petName;
	@NotNull
	@Min(2)
	@Max(15)
	@Column(name = "PET_AGE")
	private int petAge;
	@NotNull
	@Size(min = 3, max =12, message = "PetPlace should be in 3 to 12 Characters")
	private String petPlace;
	@Column(name = "PET_OWNERID")
	private Long ownerId;
	
}
