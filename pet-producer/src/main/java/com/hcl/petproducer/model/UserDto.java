package com.hcl.petproducer.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class UserDto implements Serializable {
	
	private static final long serialVersionUID = 2981865999811422120L;

	private Long userId;

	private String userName;

	private String password;

}
