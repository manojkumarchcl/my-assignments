package com.main;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.dao.UserDao;
import com.model.User;

/**
 * @author manoj.kumarc
 *
 */
public class MainUserJPA {

	public static void main(String[] args) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("demo1_persist_xml");
		EntityManager entityManager = entityManagerFactory.createEntityManager();

	UserDao userDao = new UserDao(entityManager);
	//1.Create User
//		User user = new User();
//	    user.setId(new Integer(7899)); //wrapper convert primitive data type to object(auto boxing)
//		user.setName("Vignesh");
//		user.setName("Vasu");
//		user.setName("Ichiriki");
//		user.setName("Fujimoto");
//		Optional<User> optional =  userDao.save(user);
//		if (optional.isPresent()) {
//			System.out.println("User Id: " + optional.get().getId());
//			System.out.println("User Name: " + optional.get().getName());
//		}else {
//			System.out.println("Sorry");
//		}
		
	//2. Find by Id
//		User user1 = new User();
//		user1.getId();
//		Integer id = null;
//		Optional<User> optional1 =  userDao.findById(5);
//		if (optional1.isPresent()) {
//			System.out.println("User Name: " + optional1.get().getName());
//		}else {
//			System.out.println("Sorry");
//		}
		
	//3. Read All Users
		List<User> users = userDao.findAll();
		for (User user : users) {
			System.out.println("User Id is: " +user.getId());
			System.out.println("User Name is: " +user.getName());
			
		}
	
	}

}
