package com.model;

/**
 * @author manoj.kumarc
 *
 */
public class Employee {
private int empId;
private String empName;
private float empSalary;
private String companyName;
public Employee() {
	super();
}
public Employee(int empId, String empName, float empSalary, String companyName) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.empSalary = empSalary;
	this.companyName = companyName;
}
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public float getEmpSalary() {
	return empSalary;
}
public void setEmpSalary(float empSalary) {
	this.empSalary = empSalary;
}
public String getCompanyName() {
	return companyName;
}
public void setCompanyName(String companyName) {
	this.companyName = companyName;
}

}
