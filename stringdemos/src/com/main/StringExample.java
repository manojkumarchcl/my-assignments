package com.main;

/**
 * @author manoj.kumarc
 *
 */
public class StringExample {

	public static void main(String[] args) {
		String StringExample = new String();
		String S1 = "Akeme";
		String S2 = "Goutuge";
		String S3 = "Nihon";
		String S4 = "Akeme";
		String S5 = S1;
		
		if(S5 == S1) {
			System.out.println("They are the same" +"\n");
		}else {
			System.out.println("They are different" +"\n");
		}
		if(S4 == S1) {
			System.out.println("They are the same" +"\n");
		}else {
			System.out.println("They are different" +"\n");
		}
		if(S2 == S1) {
			System.out.println("They are the same" +"\n");
		}else {
			System.out.println("They are different" +"\n");
		}
		
		
		System.out.println("\n" +S1==S5 +"\n");
		System.out.println("\n" +S1.equals(S5) +"\n");
		
		System.out.println(S1.equals(S3));
		System.out.println(S1.equals(S4));
		
		System.out.println(S1==S3);
		System.out.println(S1==S4);
		
		System.out.println(S1.compareTo(S4));
		System.out.println(S1.compareTo(S3));
		System.out.println("Compare: "+S1.compareTo(S2));
		System.out.println("Ignoring Cases" +S1.equalsIgnoreCase(S4));
		
		StringBuffer var1 = new StringBuffer("Code");
		StringBuffer var2 = new StringBuffer("788899");
		System.out.println(var1.equals(var2));
		System.out.println(var1.append(var2));
		System.out.println("Before Append:" +var1.capacity());
		System.out.println("Before Append:" +var2.capacity());
		System.out.println("After append:" +var1.capacity());
		System.out.println("After append:" +var2.capacity());
		System.out.println(var1.reverse());
		System.out.println("After Reverse:" +var1.capacity());
		
		

	}

}
