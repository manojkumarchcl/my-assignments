package com.main;

import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public class EmployeeDao {
	public static void main(String[] args) {
		Employee employee = new Employee();
		employee.setEmpId(6778);
		employee.setEmpName("Manoj");
		employee.setEmpSalary(7790.00f);
		employee.setCompanyName("HCL");

		Employee employee2 = new Employee();
		employee2.setEmpId(6799);
		employee2.setEmpName("Vasu");
		employee2.setEmpSalary(6587.00f);
		employee2.setCompanyName("TCS");

		Employee employee3 = new Employee();
		employee3.setEmpId(6778);
		employee3.setEmpName("Jason");
		employee3.setEmpSalary(76790.00f);
		employee3.setCompanyName("HCL");
		
		Employee employee4 = new Employee();
		employee3.setEmpId(6778);
		employee3.setEmpName("Jason");
		employee3.setEmpSalary(76790.00f);
		employee3.setCompanyName("HCL");
		
		System.out.println(employee3.getCompanyName().equals(employee4.getCompanyName()));
		System.out.println(employee3.getCompanyName() == employee4.getCompanyName());
		System.out.println(employee.getCompanyName().equals(employee2.getCompanyName()));
		System.out.println(employee4.getEmpId() == employee3.getEmpId());
		System.out.println(employee4.getEmpSalary() == employee3.getEmpSalary());
		
		System.out.println(employee3.getEmpName() == employee4.getEmpName());
		System.out.println(employee.getCompanyName().compareTo(employee.getEmpName()));
	
		StringBuffer sb1 = new StringBuffer("Manoj");
		StringBuffer sb2 = new StringBuffer(" HCL");
		System.out.println(sb1.equals(sb2));
		System.out.println(sb1.reverse());
		System.out.println(sb1.append(sb2));
		System.out.println(sb1.append(sb2));
		
		
	}

}
