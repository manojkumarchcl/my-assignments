package com.eurekaclient1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "hello")
public class AppController {
	@GetMapping(value = "/world")
	public ResponseEntity<String> sayHello() {
		return new ResponseEntity<String>("Welcome to Eureka Client :", HttpStatus.OK);
	}
}
