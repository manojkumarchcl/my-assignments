package com.consumerfeign.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.consumerfeign.model.EmployeeDto;

//@FeignClient(name = "simple-ref", url = "http://localhost:9095/employee")
@FeignClient(name = "producer/employee")
public interface ProducerClient {
	@GetMapping(value = "hi")
	public abstract ResponseEntity<String> check();

	@GetMapping(value = "readEmployee")
	public abstract List<EmployeeDto> getAllEmployees();

	@PostMapping(value = "createEmployee")
	public abstract ResponseEntity<EmployeeDto> addEmployee(@RequestBody EmployeeDto employeeDto);

	@GetMapping(value = "readEmployee/{deptId}")
	public abstract List<EmployeeDto> anyFunctionName(@PathVariable("deptId") int userData);
}
