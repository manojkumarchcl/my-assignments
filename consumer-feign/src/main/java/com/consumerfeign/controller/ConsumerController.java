package com.consumerfeign.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.consumerfeign.model.Department;
import com.consumerfeign.model.EmployeeDto;
import com.consumerfeign.service.ProducerClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping(value="Department")
public class ConsumerController {
@Autowired
	private ProducerClient producerClient;
	
	@GetMapping(value="/consume")
	@HystrixCommand(fallbackMethod = "sorry")
public ResponseEntity<String> getFromProducer(){
	HttpHeaders headers = new HttpHeaders();
	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	HttpEntity<String> entity = new HttpEntity<String>(headers);
//String fromProducer =restTemplate.exchange("http://localhost:9091/employee/hi", HttpMethod.GET, entity, String.class).getBody();
	
	return producerClient.check();
}
	@PostMapping(value="createDepartment")
	public ResponseEntity<Department> createEmployee(@RequestBody Department department){
		System.out.println("Consumer DeptNo:" +department.getDeptNo());
		System.out.println("Consumer DeptName: " +department.getDeptName());
		System.out.println("Consumer Details: " +department.getEmployeeDtos().get(0).getEmpName());
		return new ResponseEntity<Department>(department, HttpStatus.OK);	
	}
	@GetMapping(value="readDepartment")
	public ResponseEntity<List<Department>> readAllDepartment(){
//		List<EmployeeDto> employees1 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/1", ArrayList.class);
		List<EmployeeDto> employees1 = (List<EmployeeDto>)producerClient.anyFunctionName(1);
		Department department1 = new Department(1, "Development", employees1);
//		List<EmployeeDto> employees2 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/2", ArrayList.class);
		List<EmployeeDto> employees2 = (List<EmployeeDto>)producerClient.anyFunctionName(2);
		Department department2 = new Department(1, "Testing", employees2);
		List<Department> departments = new ArrayList<Department>();
		departments.add(department1);
		departments.add(department2);
		return new ResponseEntity<List<Department>>(departments, HttpStatus.OK);
		
	}

	@GetMapping(value="readDepartmentByid/{deptId}")
	public ResponseEntity<Department> readDepartmentByDeptId(@PathVariable int deptId){
//		List<EmployeeDto> employeeList = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/"+deptId, ArrayList.class);
		List<EmployeeDto> employeeList = producerClient.getAllEmployees();
		Department department1 = new Department(1,"Training", employeeList);
		return new ResponseEntity<Department>(department1, HttpStatus.OK);
	}

public ResponseEntity<String> sorry(){
	return new ResponseEntity<String>("Please come", HttpStatus.EXPECTATION_FAILED);
	
}
}
