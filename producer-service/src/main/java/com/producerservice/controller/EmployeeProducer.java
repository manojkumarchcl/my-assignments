package com.producerservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.producerservice.model.Employee;



@RestController
@RequestMapping(value="employee")
public class EmployeeProducer {
	@GetMapping(value="hi")
public ResponseEntity<String> sayHello(){
	return new ResponseEntity<String>("Hello from producer",HttpStatus.OK);
	
}
	@PostMapping(value="createEmployee")
	public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee){
		System.out.println("producer EmpNo:" +employee.getEmpNo());
		System.out.println("producer EmpName: " +employee.getEmpName());
		System.out.println("producer Salary: " +employee.getSalary());
		System.out.println("producer DeptId: " +employee.getDeptId());
		return new ResponseEntity<Employee>(employee, HttpStatus.OK);	
	}
	@GetMapping(value="readEmployee")
	public ResponseEntity<List<Employee>> readAllEmployee(){
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee(10,"Ten",1010.10f,1));
		employees.add(new Employee(20,"Twenty",2020.20f,2));
		employees.add(new Employee(30,"Thirty",3030.30f,1));
		employees.add(new Employee(40,"Forty",4040.40f,2));
		return new ResponseEntity<List<Employee>>(employees, HttpStatus.OK);
	}
	
	@GetMapping(value="readEmployee/{deptId}")
	public ResponseEntity<List<Employee>> readEmployeeByDeptId(@PathVariable int deptId){
	List<Employee> employeeList = null;
		if(deptId == 1) {
			List<Employee> employees = new ArrayList<Employee>();
			employees.add(new Employee(10,"Ten",1010.10f,1));
			employees.add(new Employee(30,"Thirty",3030.30f,1));
			employeeList = employees;
		}
		if(deptId==2) {
			List<Employee> employees = new ArrayList<Employee>();
			employees.add(new Employee(20,"Twenty",2020.20f,2));
			employees.add(new Employee(40,"Forty",4040.40f,2));
			employeeList = employees;
		}
		return new ResponseEntity<List<Employee>>(employeeList, HttpStatus.OK);
	}
	
}
