package com.main;

import com.model.Department;
import com.model.Employee;


/**
 * @author manoj.kumarc
 *
 */
public class EmployeeMain {
	public static void main(String[] args) {
		Employee employee1 = new Employee("Manoj", 5776788);
		Employee employee2 = new Employee("Vasu", 98989);
		Employee employee3 = new Employee("Jason", 789978);
		Employee[] employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;

		Department department = new Department();
		department.setDepartmentId(878989);
		department.setDepartmentName("Development");
		department.setEmployee(employees);
		
		Employee[] employeeArray = department.getEmployee();
		for (int i = 0; i < employeeArray.length; i++) {
			System.out.println("The Employee in Development:"+"\n");
			System.out.println("Employee Name: "+employeeArray[i].getEmpId());
			System.out.println("Employee Name: " +employeeArray[i].getEmployeeName());
		
		}
		
	}
}

