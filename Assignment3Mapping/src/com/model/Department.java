package com.model;

/**
 * @author manoj.kumarc
 *
 */
public class Department {

	private String departmentName;
	private int departmentId;
	public Employee[] employee;
	public Department() {
		super();
	}
	public Department(String departmentName, int departmentId, Employee[] employee) {
		super();
		this.departmentName = departmentName;
		this.departmentId = departmentId;
		this.employee = employee;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public Employee[] getEmployee() {
		return employee;
	}
	public void setEmployee(Employee[] employee) {
		this.employee = employee;
	}

}
