package com.main;

import java.util.Arrays;

public class ArrayMain {
	public static void main(String[] args) {
		int[] someArray = new int[5];
		someArray[0] = 50;
		someArray[1] = 80;
		someArray[2] = 59;
		someArray[3] = 72;
		someArray[4] = 80;
		int sum = 0;
		System.out.println("The Array is:" + Arrays.toString(someArray));
		for (int i : someArray) {
			sum += i;
		}
		System.out.println("The Sum of the Array is: " + sum);
	}
}
