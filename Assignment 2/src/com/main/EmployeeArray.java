package com.main;

import com.model.Employee;

public class EmployeeArray {
	public static void main(String[] args) {
		Employee employee1 = new Employee(5666677,"Mike", 69998f);
		Employee employee2 = new Employee(5678899,"Mark", 69978f);
		Employee employee3 = new Employee(5666677,"Bill", 69867f);
	
		int[] someArray = new int[3];
		Employee[] employees = new Employee[3];
		employees[0] = employee1;
		employees[1] = employee2;
		employees[2] = employee3;
		int sum = 0;
		
		for(int i: someArray) {
			sum += i;
			System.out.println("The Employee Salaries are:");
			System.out.println("The sum of the salaries is: " +sum);
		}
		
	
	}

}
