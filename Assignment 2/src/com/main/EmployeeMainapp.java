package com.main;

import com.model.Employee;

public class EmployeeMainapp {
	public static void main(String[] args) {
		Employee employee = new Employee();
		System.out.println("The Employee Number is: " + employee.getEmployeeNumber());
		System.out.println("The Employee Name is: " + employee.getEmployeeName());
		System.out.println("The Employee Number is:" + employee.getEmployeeSalary());

		Employee employee2 = new Employee(45566, "Bill", 656798f);
		System.out.println("The Employee Number is: " + employee2.getEmployeeNumber());
		System.out.println("The Employee Name is: " + employee2.getEmployeeName());
		System.out.println("The Employee Salary is: " + employee2.getEmployeeSalary());
		System.out.println(
				"The highest Salary is: " + Math.max(employee.getEmployeeSalary(), employee2.getEmployeeSalary()));
		if(employee.getEmployeeSalary()>employee2.getEmployeeSalary()) {
			System.out.println("The highest drawn salary is:"+employee2.getEmployeeName());
		}else {
			System.out.println("lowest salary");
		}
			
		employee = null;
		employee2 = null;

	}
}
