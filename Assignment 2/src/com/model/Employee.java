package com.model;

public class Employee {

	private int employeeNumber;
	private String employeeName;
	private float employeeSalary;

	public Employee() {
		this.employeeNumber = 5132566;
		this.employeeName = "Mike";
		this.employeeSalary = 50000f;
	}

	public Employee(int employeeNumber1, String employeeName2, float employeeSalary2) {
		this.employeeNumber = employeeNumber1;
		this.employeeName = employeeName2;
		this.employeeSalary = employeeSalary2;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(float employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

}
