package com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Department;
import com.model.Employee;
import com.util.HibernateUtil;

public class DepartmentDaoImpl implements DepartmentDao {

	@Override
	public Department createDepartment(Department department) {
		Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		transaction = session.beginTransaction();
		session.save(department);
		 transaction.commit();
		return department;
	}

	@Override
	public Department readDepartmentById(int deptId) {
		Transaction transaction = null;
		Department department= null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			department = session.get(Department.class, deptId);
			 transaction.commit();
		} catch (HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return department;
	}


	@Override
	public Department updateDepartment(Department department) {
		Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		transaction = session.beginTransaction();
		session.update(department);
		 transaction.commit();
		return department;
	}

	@Override
	public int deleteDepartmentById(int deptId) {
		Transaction transaction = null;
		Department department = null;
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			department = session.get(Department.class, deptId);
			if(department != null) {
				System.out.println("Department is deleted");
			}
			 transaction.commit();
		} catch (HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return 1;
	}

}
