package com.dao;

import com.model.Department;


public interface DepartmentDao {
	public abstract Department createDepartment(Department department);
	public abstract Department readDepartmentById(int deptId);
	public abstract Department updateDepartment(Department department);
	public abstract int deleteDepartmentById(int deptId);
}
