package com.dao;

import com.model.Employee;

public interface EmployeeDao {

	public abstract Employee createEmployee(Employee employee);
	public abstract Employee readEmployeeById(int empId);
	public abstract Employee updateEmployee(Employee employee);
	public abstract int deleteEmployeeById(int empId);
	
}
