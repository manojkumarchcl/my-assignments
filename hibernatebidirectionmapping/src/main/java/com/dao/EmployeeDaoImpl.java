package com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.model.Employee;
import com.util.HibernateUtil;

public class EmployeeDaoImpl implements EmployeeDao {

	@Override
	public Employee createEmployee(Employee employee) {
		Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		transaction = session.beginTransaction();
		session.save(employee);
		 transaction.commit();
		return employee;
	}

	@Override
	public Employee readEmployeeById(int empId) {
		Transaction transaction = null;
		Employee employee = null;
		try {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			employee = session.get(Employee.class, empId);
			 transaction.commit();
		} catch (HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return employee;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		Transaction transaction = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		transaction = session.beginTransaction();
		session.update(employee);
		 transaction.commit();
		return employee;
	}

	@Override
	public int deleteEmployeeById(int empId) {
		Transaction transaction = null;
		Employee employee = null;
		try {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			transaction = session.beginTransaction();
			employee = session.get(Employee.class, empId);
			if(employee != null) {
				System.out.println("Employee is deleted");
			}
			 transaction.commit();
		} catch (HibernateException e) {
			if(transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return 1;

	}
	}
