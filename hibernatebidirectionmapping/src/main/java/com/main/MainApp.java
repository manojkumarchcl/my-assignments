package com.main;

import java.util.Scanner;

import com.dao.DepartmentDao;
import com.dao.DepartmentDaoImpl;
import com.dao.EmployeeDao;
import com.dao.EmployeeDaoImpl;
import com.model.Department;
import com.model.Employee;

public class MainApp {

	public static void main(String[] args) {
		EmployeeDao employeeDao = new EmployeeDaoImpl();
		DepartmentDao departmentDao = new DepartmentDaoImpl();

		Department department = new Department("Java Development");
		departmentDao.createDepartment(department);

		Department department2 = new Department("UX Design");
		departmentDao.createDepartment(department2);
		
		Department department3 = new Department("Full Stack Developement");
		departmentDao.createDepartment(department3);
		
		Department department4 = new Department("SAP ABAP");
		departmentDao.createDepartment(department4);
		
		Department department5 = new Department("Japanese");
		departmentDao.createDepartment(department5);
		

		/*Employee employee = new Employee("Manoj", 76888.00f);
		employee.setDepartment(department);
		employeeDao.createEmployee(employee);

		Employee employee1 = new Employee("Jason", 76866.00f);
		employee1.setDepartment(department2);
		employeeDao.createEmployee(employee1);*/
		
		Employee employee3 = new Employee("Vasu", 768677.00f);
		employee3.setDepartment(department4);
		employeeDao.createEmployee(employee3);
		
		Employee employee4 = new Employee("Ichiriki", 7686799.00f);
		employee4.setDepartment(department5);
		employeeDao.createEmployee(employee4);
		
		Employee employee5 = new Employee("Faiz", 7686788.00f);
		employee5.setDepartment(department3);
		employeeDao.createEmployee(employee5);
		
		Employee employee6 = new Employee("Vicky", 7686788.00f);
		employee6.setDepartment(department);
		employeeDao.createEmployee(employee6);
		
		Employee employee7 = new Employee("Manoj Kumar", 7686788.00f);
		employee7.setDepartment(department2);
		employeeDao.createEmployee(employee7);
	}

}
