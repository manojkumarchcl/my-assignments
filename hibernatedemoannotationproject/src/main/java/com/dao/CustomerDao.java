package com.dao;

import java.util.List;

import com.bean.Customers;

/**
 * @author manoj.kumarc
 *
 */
public interface CustomerDao {
public abstract Customers createCustomers(Customers customers);
public abstract List<Customers> readAllCustomers();
public abstract Customers readCustomersById(int custId);
public abstract Customers readCustomersByName(String custName);
public abstract Customers updateCustomers(Customers customers);
public abstract int deleteCustomersById(int custId);
public abstract String deleteCustomersByName(String custName);
}
