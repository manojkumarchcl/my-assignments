package com.dao;

import java.util.List;
 
import org.hibernate.Session;
import org.hibernate.Transaction;
 
import com.bean.Customers;
import com.util.HibernateUtil;
 
/**
 * @author manoj.kumarc
 *
 */
public class CustomerDaoImpl implements CustomerDao {
 
    @Override
    public Customers createCustomers(Customers customers) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.save(customers);
        transaction.commit();
        return customers;
 
    }
 
    @Override
    public List<Customers> readAllCustomers() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        String readAllCustomers = "from customer_data";
        Transaction transaction = session.beginTransaction();
        List<Customers> customer = session.createQuery(readAllCustomers).list();
        transaction.commit();
        return customer;
    }
 
    @Override
    public Customers readCustomersById(int custId) {
 
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Customers customers = session.get(Customers.class, custId);
        transaction.commit();
        return customers;
    }
 
    @Override
    public Customers readCustomersByName(String custName) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Customers customers = session.get(Customers.class, custName);
        transaction.commit();
        return customers;
    }
 
    @Override
    public Customers updateCustomers(Customers customers) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(customers);
        return customers;
    }
 
    @Override
    public int deleteCustomersById(int custId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.delete(session.get(Customers.class, custId));
        return 1;
    }
 
    @Override
    public String deleteCustomersByName(String custName) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.delete(session.get(Customers.class, custName));
        return null;
    }
 
}