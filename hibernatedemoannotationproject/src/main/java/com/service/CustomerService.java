package com.service;

import java.util.List;

import com.bean.Customers;

/**
 * @author manoj.kumarc
 *
 */
public interface CustomerService {
	
		public abstract Customers validatecreateCustomers(Customers customers);
		public abstract List<Customers> validatereadAllCustomers();
		public abstract Customers validatereadCustomersById(int custId);
		public abstract Customers validatereadCustomersByName(String custName);
		public abstract Customers validateupdateCustomers(Customers customers);
		public abstract int validatedeleteCustomersById(int custId);
		public abstract String validatedeleteCustomersByName(String custName);
}
