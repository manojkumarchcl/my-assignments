package com.service;

import java.util.List;

import javax.transaction.Transactional;

import com.bean.Customers;
import com.dao.CustomerDao;
import com.dao.CustomerDaoImpl;


/**
 * @author manoj.kumarc
 *
 */
public class CustomerServiceImpl implements CustomerService {

	@Override
	
	public Customers validatecreateCustomers(Customers customers) {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.createCustomers(customers);
		
	
	}

	@Override
	public List<Customers> validatereadAllCustomers() {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.readAllCustomers();
	}

	@Override
	public Customers validatereadCustomersById(int custId) {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.readCustomersById(custId);
	}

	@Override
	public Customers validatereadCustomersByName(String custName) {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.readCustomersByName(custName);
	}

	@Override
	public Customers validateupdateCustomers(Customers customers) {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.updateCustomers(customers);
	}

	@Override
	public int validatedeleteCustomersById(int custId) {
		CustomerDao customerDao = new CustomerDaoImpl();
		return customerDao.deleteCustomersById(custId);
	}

	@Override
	public String validatedeleteCustomersByName(String custName) {
		// TODO Auto-generated method stub
		return null;
	}
	}
