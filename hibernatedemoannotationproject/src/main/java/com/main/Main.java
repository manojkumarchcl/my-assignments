package com.main;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
 
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
 
import com.bean.Customers;
import com.dao.CustomerDao;
import com.dao.CustomerDaoImpl;
 
/**
 * @author manoj.kumarc
 *
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CustomerDao customerDao = new CustomerDaoImpl();
        Customers customers = new Customers();
        boolean exit = false;
        while (!exit) {
            System.out.println("The options are:" + "\n");
            System.out.println("1. Create a Customer:" + "\n");
            System.out.println("2. Fetch Customer By Id:" + "\n");
            System.out.println("3. Fetch Customer by Name:" + "\n");
            System.out.println("4. Delete a Customer:" + "\n");
            System.out.println("5. Fetch All Customers:" + "\n");
            System.out.println("6. Update the Customers:" + "\n");
            System.out.println("Select Options:");
            int option = scanner.nextInt();
            try {
                switch (option) {
                case 1:
                    System.out.println("You Have Selected option 1: Create a Customer:");
                    System.out.println("Enter the Customer Name: ");
                    String name = scanner.next();
                    customers.setCustomerName(name);
                    System.out.println("Enter the Customer Balance: ");
                    float balance = scanner.nextFloat();
                    customers.setBalance(balance);
                    customerDao.createCustomers(customers);
                    System.out.println("Customer Id: " + customers.getCustomerId());
                    System.out.println("Customer Name: " + customers.getCustomerName());
                    System.out.println("Customer Balance: " + customers.getBalance());
                    System.out.println("Connection Closed" + "\n");
                    break;
 
                case 2:
                    System.out.println("\n" + "You have selected option 2: Fetch Customer By Id:");
                    System.out.println("Enter the Customer Id: ");
                    int custId = scanner.nextInt();
                    customers.setCustomerId(custId);
                    customerDao.readCustomersById(custId);
                    System.out.println("Customer Id is: " + customers.getCustomerId());
                    System.out.println("Customer Name is: " + "\n" + customers.getCustomerName());
                    System.out.println("Customer Balance is: " + "\n" + customers.getBalance());
                    System.out.println("Connection Closed" + "\n");
                    break;
                 case 3:
                 System.out.println("\n" + "You have selected option 3: Fetch Customer Name:");
                 System.out.println("Enter the Employee Name:");
                 String custName = scanner.next();
                 customerDao.readCustomersByName(custName);
                 System.out.println("Customer Name is: " + "\n" + customers.getCustomerName());
                 System.out.println("Customer Id is: " + "\n" + customers.getCustomerId());
                 System.out.println("Customer Balance is: " + "\n" + customers.getBalance());
                 System.out.println("Connection Closed" + "\n");
                 break;
                 case 4:
                 System.out.println("You have selected option 4: Delete a Customer:");
                 System.out.println("Enter the Employee Id:");
                 int id4 = scanner.nextInt();
                 int i = customerDao.deleteCustomersById(id4);
                 if (i == 1) {
                 System.out.println("\n" + "The employee has been deleted" + "\n");
                 } else {
                 System.out.println("Oops");
                 }

                 System.out.println("Connection Closed" + "\n");
                 break;
                 case 5:
                 System.out.println("You have selected option: Fetch All Customers:");
                 List<Customers> readAllCustomers =
                 customerDao.readAllCustomers();
                 for (Customers customer : readAllCustomers) {
                 System.out.println("Customer Id is:" + customer.getCustomerId());
                 System.out.println("Customer Name is:" + customer.getCustomerName());
                 System.out.println("Customer Balance is:" + customer.getBalance());
                 }
                 System.out.println("Connection Closed" + "\n");
                 break;
                 case 6:
                 System.out.println("You have selected option: Update Customer name:");
                 System.out.println("Enter the Customer Name: ");
                 String name6 = scanner.next();
                 customers.setCustomerName(name6);
                 System.out.println("Enter the updated Customer Name: ");
                 String name7 = scanner.next();
                 customers.setCustomerName(name7);
                 System.out.println("Customer Id is:" + customers.getCustomerId());
                 System.out.println("Customer Name is:" + customers.getCustomerName());
                 System.out.println("Customer Balance is:" + customers.getBalance());
                 System.out.println("Connection Closed" + "\n");
                 break;
                default:
                    System.out.println("Only numbers between 1 to 6");
 
                }
 
            } catch (InputMismatchException e) {
                System.out.println("You must insert a number");
                scanner.next();
            }
 
        }
 
    }
}