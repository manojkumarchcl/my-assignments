package com.service;

import com.model.Customers;

/**
 * @author manoj.kumarc
 *
 */
public interface CustomerService {

	public abstract Customers validateUserIdAndPassword(int customerId, String name);
	
}
