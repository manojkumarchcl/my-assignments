package com.model;

/**
 * @author manoj.kumarc
 *
 */
public class Customers {
private int customerId;
private String customerName;
private int salary;
public Customers() {
	super();
}
public Customers(int customerId, String customerName, int salary) {
	super();
	this.customerId = customerId;
	this.customerName = customerName;
	this.salary = salary;
}
public int getCustomerId() {
	return customerId;
}
public void setCustomerId(int customerId) {
	this.customerId = customerId;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public float getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary = salary;
}

}
