package com.springbootdemo1.service;

import com.springbootdemo1.model.Address;

public interface AddressService {

	public abstract Address addAddress(Address address);
	public abstract Address readAddressByDoorNo(int doorNo);
	public abstract Address readAddressByCity(String city);
	public abstract Address readAddressByState(String state);
	public abstract Address alterAddress(Address address);
	public abstract int removeAddressByDoorNo(int doorNo);
	
	
}
