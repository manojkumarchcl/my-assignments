package com.springbootdemo1.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springbootdemo1.model.Address;
import com.springbootdemo1.repository.AddressRepository;
@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private AddressRepository addressRepository;

	@Override
	@Transactional
	public Address addAddress(Address address) {
		// //logic to persist in database
		// System.out.println("Door no in Service Layer: " +address.getDoorNo());
		// System.out.println("City in Service Layer: " +address.getCity());
		// System.out.println("State in Service Layer: " +address.getState());
		// //process
		// address.setCity(address.getCity() +"Processed");
		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public Address readAddressByDoorNo(int doorNo) {
		// Address address = new Address();
		// if(doorNo == 100) {
		// address = new Address(100, "Chennai", "TN");
		// }
		// if(doorNo == 200) {
		// address = new Address(200, "Bangalore", "KA");
		// }
		Address address = null;
		Optional<Address> optionalAddress = addressRepository.findById(doorNo);
		if (optionalAddress.isPresent()) {
			address = optionalAddress.get();
		}
		return address;
	}

	@Override
	public Address readAddressByCity(String city) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address readAddressByState(String state) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Address alterAddress(Address address) {
//		System.out.println("Door no in Service Layer: " + address.getDoorNo());
//		System.out.println("City in Service Layer: " + address.getCity());
//		System.out.println("State in Service Layer: " + address.getState());
//		// process
//		address.setCity(address.getCity() + "updates");
//		return address;
		return addressRepository.save(address);
	}

	@Override
	@Transactional
	public int removeAddressByDoorNo(int doorNo) {
		addressRepository.deleteById(doorNo);
		return 1;
	}

}
