package com.springbootdemo1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.springbootdemo1.model.Address;
@Repository
public interface AddressRepository extends CrudRepository<Address, Integer>{


}
