package com.springbootdemo1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo1.model.Address;
import com.springbootdemo1.service.AddressService;

@RestController
public class AddressController {
	@Autowired
	private AddressService addressService;
@GetMapping(value = "/")
public String sayHello() {
	return "Welcome to Spring Boot";
}

@PostMapping(value = "create")
	public ResponseEntity<Address> createAddress(@RequestBody Address address){
		Address dummyAddress = null;
		if(address != null) {
			dummyAddress = addressService.addAddress(address);

		}
		return new ResponseEntity<Address> (dummyAddress, HttpStatus.CREATED);
}
@GetMapping(value = "readbyid/{doorNo}")
public Address readAddressByDoorNo(@PathVariable int doorNo){
	Address address = null;
	if(doorNo>0) {
		address = addressService.readAddressByDoorNo(doorNo);
		
	}
	return address;	
}
@PutMapping(value = "/update")
public Address updateAddress(@RequestBody Address address){
	
	return addressService.alterAddress(address);
}	
	
@DeleteMapping(value = "deletebyid/{doorNo}")
public int deleteAddress(@PathVariable int doorNo){

	return addressService.removeAddressByDoorNo(doorNo);	
}	
}

