package com.springbootdemo1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Spring_data")
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int doorNo;
	@Column(name = "City", length = 30)
	private String city;
	@Column(name = "state_code", length = 30)
	private String state;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(int doorNo, String city, String state) {
		super();
		this.doorNo = doorNo;
		this.city = city;
		this.state = state;
	}

	public int getDoorNo() {
		return doorNo;
	}

	public void setDoorNo(int doorNo) {
		this.doorNo = doorNo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
