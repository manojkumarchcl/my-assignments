package com.hcl.pp.userconsumer.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PetsDto implements Serializable {
	
	private static final long serialVersionUID = 2099733336682612853L;
	private Long petId;
	private String petName;
	private int petAge;
	private String petPlace;
	private Long ownerId;
}
