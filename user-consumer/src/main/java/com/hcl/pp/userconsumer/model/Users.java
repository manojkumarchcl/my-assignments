package com.hcl.pp.userconsumer.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "USER")
public class Users implements Serializable{

	private static final long serialVersionUID = 5395781490420353879L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "USER_ID")
	private Long userId;
	@Column(name = "USER_NAME",length = 55, nullable = false, unique = true)
	private String userName;
	@Column(name = "PASSWORD",length = 55, nullable = false)
	private String password;
	@Transient
	private String confirmPassword;
	
}
