package com.hcl.pp.userconsumer.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Optional;
import com.hcl.pp.userconsumer.exception.UserConsumerException;
import com.hcl.pp.userconsumer.model.PetsDto;
import com.hcl.pp.userconsumer.model.Users;
import com.hcl.pp.userconsumer.service.ProducerClient;
import com.hcl.pp.userconsumer.service.UserConsumerService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.userconsumer.validator.LoginValidator;
import com.userconsumer.validator.UserValidator;

import ch.qos.logback.core.net.SyslogOutputStream;
import lombok.extern.slf4j.Slf4j;

/**
 * @author manoj.kumarc
 *
 */
@RestController
@Slf4j
@RequestMapping(value = "usersconsumers")
@Validated
public class UserConsumerController {
	@Autowired
	private UserConsumerService userConsumerService;

	@PostMapping(value = "createusers")
	public ResponseEntity<Object> createUsers(@RequestBody @Valid UserValidator userRequest)
			throws UserConsumerException {
		Users users = null;
		ResponseEntity<Object> responseEntity = null;

		try {
			users = userConsumerService.createUser(userRequest);
			if (users != null) {
				responseEntity = new ResponseEntity<Object>(users, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(users, HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			log.error("{}", e.getMessage());
			e.getMessage();
			responseEntity = new ResponseEntity<Object>(users, HttpStatus.BAD_REQUEST);

		}
		log.info("The User Id is: " + users.getUserId());
		log.info("The User Name is: " + users.getUserName());
		log.info("The User Password is: " + users.getPassword());

		return responseEntity;

	}

	@PostMapping("/login")
	public ResponseEntity<Object> loginUser(@Valid @RequestBody LoginValidator loginRequest)
			throws UserConsumerException {
		List<PetsDto> pets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			pets = userConsumerService.loginUser(loginRequest);
			if (pets != null) {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.OK);
			} else {
				responseEntity = new ResponseEntity<Object>(pets, HttpStatus.NOT_FOUND);
			}
		} catch (UserConsumerException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	@GetMapping(value = "readallusers")
	public ResponseEntity<List<Users>> readUsers() throws UserConsumerException {
		List<Users> users = null;
		users = userConsumerService.readAllUsers();
		return new ResponseEntity<List<Users>>(users, HttpStatus.OK);
	}

	 @GetMapping(value = "readbyuserid/{userId}")
	 @HystrixCommand(fallbackMethod = "idNotFound")
	 public Users readPetByUserId(@PathVariable Long userId) throws UserConsumerException {
	 log.info("The UserId is: " + userId);
	 Users users = null;
	 if (userId > 0) {
		 users = userConsumerService.readUserById(userId);
		 log.info("The User Id is:" + users.getUserId());
	 }
	 return users;
	 }

	@PutMapping(value = "/updateuser")
	public Users alterUser(@RequestBody Users users) throws UserConsumerException {
		log.info("Updated User Id is:" + users.getUserId());
		log.info("Updated User Name is:" + users.getUserName());
		log.info("Updated User pwd is :" + users.getPassword());
		return userConsumerService.updateUser(users);
	}

	@DeleteMapping(value = "deletebyuserid/{userId}")
	public Long deleteUser(@PathVariable Long userId) throws UserConsumerException {
		log.info("Deleted The UserId is: " + userId);
		try {
			return userConsumerService.deleteUserById(userId);
		} catch (org.springframework.dao.EmptyResultDataAccessException e) {
			e.printStackTrace();
		}

		return userId;
	}

	@PutMapping("/buy/pet/{userId}/{petId}")
	@HystrixCommand(fallbackMethod = "serviceNotAvailable")
	public ResponseEntity<Object> buyPet(
			@PathVariable("userId") @Min(value = 1, message = "Minimum entry must be One") Long userId,
			@PathVariable("petId") @Min(value = 1, message = "Minimum entry must be One") Long petId)
			throws UserConsumerException {
		PetsDto userBoughtPets = null;
		ResponseEntity<Object> responseEntity = null;
		try {
			userBoughtPets = userConsumerService.buyPet(userId, petId);
			if (userBoughtPets != null) {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CREATED);
			} else {
				responseEntity = new ResponseEntity<Object>(userBoughtPets, HttpStatus.CONFLICT);
			}
		} catch (UserConsumerException e) {
			log.error("{}", e.getMessage());
			responseEntity = new ResponseEntity<Object>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}

	public ResponseEntity<Object> serviceNotAvailable(Long userId, Long petId) {
		return new ResponseEntity<Object>("Service Not Available", HttpStatus.EXPECTATION_FAILED);
	}
	
	public Long idNotFound(Long userId) {
		 log.info("The id is not available");
				 return userId;
	}
}
