package com.hcl.pp.userconsumer.exception;

/**
 * @author manoj.kumarc
 *
 */
public class UserConsumerException extends Exception {
	
	private static final long serialVersionUID = -7579658947742895784L;
	
	private String message;

	public UserConsumerException(String message) {
		super();
		this.message = message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}

}
