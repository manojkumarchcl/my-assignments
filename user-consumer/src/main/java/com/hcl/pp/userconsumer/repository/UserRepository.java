package com.hcl.pp.userconsumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pp.userconsumer.model.Users;
import com.userconsumer.validator.UserValidator;
/**
 * @author manoj.kumarc
 *
 */
@Repository
public interface UserRepository extends JpaRepository<Users, Long>{
	
	public Users findByUserName(String userName);
	
	public Users findByUserNameAndPassword(String name, String password);


}
