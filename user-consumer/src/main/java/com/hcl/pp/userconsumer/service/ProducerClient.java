package com.hcl.pp.userconsumer.service;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.common.base.Optional;
import com.hcl.pp.userconsumer.exception.UserConsumerException;
import com.hcl.pp.userconsumer.model.PetsDto;

/**
 * @author manoj.kumarc
 *
 */
@FeignClient(name = "producer/petproducer")
public interface ProducerClient {
	
	@GetMapping("/pet/home")
	public List<PetsDto> getAllPets() throws UserConsumerException;

	@GetMapping("/pet/user/detail/{userId}")
	public List<PetsDto> getPetsByUserId(@PathVariable("userId") Long userId) throws UserConsumerException;

	@GetMapping("/pet/detail/{petId}")
	public Optional<PetsDto> getPetById(@PathVariable("petId") Long petId) throws UserConsumerException;;

	@PutMapping("/pet/buy/pet")
	public PetsDto savePetWithUser(@RequestBody PetsDto petDto) throws UserConsumerException;

	
}
