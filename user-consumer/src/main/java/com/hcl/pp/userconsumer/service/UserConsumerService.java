package com.hcl.pp.userconsumer.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.pp.userconsumer.exception.UserConsumerException;
import com.hcl.pp.userconsumer.model.PetsDto;
import com.hcl.pp.userconsumer.model.Users;
import com.userconsumer.validator.LoginValidator;
import com.userconsumer.validator.UserValidator;
/**
 * @author manoj.kumarc
 *
 */
public interface UserConsumerService {
	
	public abstract Users createUser(UserValidator userRequest) throws UserConsumerException;

	public abstract Users readUserById(Long userId) throws UserConsumerException;

	public abstract Users updateUser(Users users) throws UserConsumerException;

	public abstract Long deleteUserById(Long userId) throws UserConsumerException;

	public abstract List<Users> readAllUsers() throws UserConsumerException;
	
	public PetsDto buyPet(Long userId, Long petId) throws UserConsumerException;
	
	public List<PetsDto> loginUser(LoginValidator loginRequest) throws UserConsumerException;


}
