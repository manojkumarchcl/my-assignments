package com.hcl.pp.userconsumer.service;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Optional;
import com.hcl.pp.userconsumer.exception.UserConsumerException;
import com.hcl.pp.userconsumer.model.PetsDto;
import com.hcl.pp.userconsumer.model.Users;
import com.hcl.pp.userconsumer.repository.UserRepository;
import com.userconsumer.validator.LoginValidator;
import com.userconsumer.validator.UserValidator;

import lombok.extern.slf4j.Slf4j;
/**
 * @author manoj.kumarc
 *
 */
@Slf4j
@Service
public class UserConsumerServiceImpl implements UserConsumerService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ProducerClient producerClient;
	
	@Override
	@Transactional
	public Users createUser(UserValidator userRequest) throws UserConsumerException {
		
		Users userCreated = null;

		if (!(userRequest.getPassword().equals(userRequest.getConfirmPassword()))) {
			throw new UserConsumerException("Passwords do not match");
		} else {
			Users userExistsWithUserName = userRepository.findByUserName(userRequest.getUserName());
			if (userExistsWithUserName != null) {
				throw new UserConsumerException("User Name already in use. Please select a different User Name");
			} else {
				try {
					ModelMapper modelMapper = new ModelMapper();
					Users userRequested = modelMapper.map(userRequest, Users.class);
					userCreated = userRepository.save(userRequested);
					if (userCreated != null) {
						return userCreated;
					} else {
						throw new UserConsumerException("User Not created");
					}
				} catch (UserConsumerException e) {
					log.error("{}", e.getMessage());
				}
			}
			return userCreated;
		}
	}

	@Override
	@Transactional
	public Users readUserById(Long userId) throws UserConsumerException {
		Users users = null;
		java.util.Optional<Users> optionalUsers = userRepository.findById(userId);
		if (optionalUsers.isPresent()) {
			users = optionalUsers.get();
		}
		return users;
	}

	@Override
	@Transactional
	public Users updateUser(Users users) throws UserConsumerException {
		// TODO Auto-generated method stub
		return userRepository.save(users);
	}

	@Override
	@Transactional
	public Long deleteUserById(Long userId) throws UserConsumerException {
		userRepository.deleteById(userId);
		return userId;
	}

	@Override
	@Transactional
	public List<Users> readAllUsers() throws UserConsumerException {
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public PetsDto buyPet(Long userId, Long petId) throws UserConsumerException {
		PetsDto petWithOwnerCreated = null;
		java.util.Optional<Users> optionalUser = userRepository.findById(userId);
		if (optionalUser.isPresent()) {
			Optional<PetsDto> pet = producerClient.getPetById(userId);
			if (pet.isPresent()) {
				if (pet.get().getOwnerId()!= null) {
					throw new UserConsumerException("Pet Already Purchased Try with another Pet(PetId)");
				} else {
					pet.get().setOwnerId(userId);
					petWithOwnerCreated = producerClient.savePetWithUser(pet.get());
					if (petWithOwnerCreated != null) {
						return petWithOwnerCreated;
					} else {
						throw new UserConsumerException("Entry Not created");
					}
				}
			} else {
				throw new UserConsumerException("Pet Not exists with Given Id");
			}
		} else {
			throw new UserConsumerException("User Not exists with Given Id");
		}

	}


	@Override
	@Transactional
	public List<PetsDto> loginUser(LoginValidator loginRequest) throws UserConsumerException {
		Users userExists = userRepository.findByUserNameAndPassword(loginRequest.getUserName(), loginRequest.getPassword());
		
		List<PetsDto> pets = null;
		if (userExists != null) {
			pets = producerClient.getAllPets();
			if (pets != null && pets.size() > 0) {
				return pets;
			} else {
				throw new UserConsumerException("No Pets Available");
			}
		} else {
			throw new UserConsumerException("Either User Name or Password or both are invalid");
		}
	}

	
}
