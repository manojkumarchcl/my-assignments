package com.userconsumer.validator;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author manoj.kumarc
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginValidator implements Serializable {
	
	private static final long serialVersionUID = 4187864350306168549L;

	@NotNull(message = "UserName should not be null")
	@NotEmpty(message = "UserName should not empty")
	private String userName;

	@NotNull(message = "Password should not null")
	@NotEmpty(message = "Password should not empty")
	private String password;


}
