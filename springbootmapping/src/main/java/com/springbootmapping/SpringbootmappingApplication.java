package com.springbootmapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootmappingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootmappingApplication.class, args);
	}

}
