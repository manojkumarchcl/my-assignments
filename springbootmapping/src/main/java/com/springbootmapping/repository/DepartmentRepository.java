package com.springbootmapping.repository;

import org.springframework.data.repository.CrudRepository;

import com.springbootmapping.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Integer> {

}
