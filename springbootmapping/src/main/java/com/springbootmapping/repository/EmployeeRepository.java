package com.springbootmapping.repository;

import org.springframework.data.repository.CrudRepository;

import com.springbootmapping.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

}
