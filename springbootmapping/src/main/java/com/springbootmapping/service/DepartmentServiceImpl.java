package com.springbootmapping.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.springbootmapping.model.Department;
import com.springbootmapping.repository.DepartmentRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {
	private DepartmentRepository departmentRepository;

	@Override
	@Transactional
	public Department createDepartment(Department department) {

		return departmentRepository.save(department);
	}

	@Override
	@Transactional
	public Department readDepartmentById(int deptId) {
		Department department = null;
		Optional<Department> optionalDepartment = departmentRepository.findById(deptId);
		if (deptId > 0) {
			department = optionalDepartment.get();
		}
		return department;
	}


	@Override
	@Transactional
	public int deleteDepartmentById(int deptId) {
		departmentRepository.deleteById(deptId);
		return 1;
	}

	@Override
	public Department updateDepartment(Department department) {
		// TODO Auto-generated method stub
		return null;
	}

}
