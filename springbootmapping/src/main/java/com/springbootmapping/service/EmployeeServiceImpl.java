package com.springbootmapping.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.springbootmapping.model.Employee;
import com.springbootmapping.repository.EmployeeRepository;
@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;

	@Override
	@Transactional
	public Employee createEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

	@Override
	@Transactional
	public Employee readEmployeeById(int empId) {
		Employee employee = null;
		Optional<Employee> optionalEmployee = employeeRepository.findById(empId);
		if (optionalEmployee.isPresent()) {
			employee = optionalEmployee.get();
		}
		return employee;
	}


	@Override
	@Transactional
	public int deleteEmployeeById(int empId) {
		employeeRepository.deleteById(empId);
		return 1;
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		// TODO Auto-generated method stub
		return null;
	}

}
