package com.springbootmapping.service;

import com.springbootmapping.model.Department;


public interface DepartmentService {

	public abstract Department createDepartment(Department department);
	public abstract Department readDepartmentById(int deptId);
	public abstract Department updateDepartment(Department department);
	public abstract int deleteDepartmentById(int deptId);
}
