package com.springbootmapping.service;

import com.springbootmapping.model.Employee;

public interface EmployeeService {
	public abstract Employee createEmployee(Employee employee);
	public abstract Employee readEmployeeById(int empId);
	public abstract Employee updateEmployee(Employee employee);
	public abstract int deleteEmployeeById(int empId);
	
}
