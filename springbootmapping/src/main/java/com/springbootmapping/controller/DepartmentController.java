package com.springbootmapping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootmapping.model.Department;
import com.springbootmapping.service.DepartmentService;

@RestController
public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;

	@PostMapping(value = "create")
	public ResponseEntity<Department> createDepartment(@RequestBody Department department) {
		Department dummyDepartment = null;
		if (department != null) {
			dummyDepartment = departmentService.createDepartment(department);

		}
		return new ResponseEntity<Department>(dummyDepartment, HttpStatus.CREATED);
	}

	@GetMapping(value = "readbyid/{deptId}")
	public Department readDepartmentById(@PathVariable int deptId) {
		Department department = null;
		if (deptId > 0) {
			department = departmentService.readDepartmentById(deptId);

		}
		return department;
	}

	@PutMapping(value = "/update")
	public Department updateDepartment(@RequestBody Department department) {

		return departmentService.updateDepartment(department);
	}

	@DeleteMapping(value = "deletebyid/{deptId}")
	public int deleteDepartmentById(@PathVariable int deptId) {

		return departmentService.deleteDepartmentById(deptId);
	}

}
