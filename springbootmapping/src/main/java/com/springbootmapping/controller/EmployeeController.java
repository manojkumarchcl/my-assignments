package com.springbootmapping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootmapping.model.Employee;
import com.springbootmapping.service.EmployeeService;

@RestController
public class EmployeeController {
@Autowired
	private EmployeeService employeeService;
@PostMapping(value = "create")
public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee){
	Employee dummyEmployee = null;
	if(employee != null) {
		dummyEmployee = employeeService.createEmployee(employee);

	}
	return new ResponseEntity<Employee>  (dummyEmployee, HttpStatus.CREATED);
}
@GetMapping(value = "readbyid/{empId}")
public Employee readById(@PathVariable int empId){
	Employee employee = null;
	if(empId>0) {
		employee = employeeService.readEmployeeById(empId);	
	}
	return employee;
	
}
@PutMapping(value = "/update")
public Employee updateEmployee(@RequestBody Employee employee){
	
	return employeeService.updateEmployee(employee);
	
}
@DeleteMapping(value = "deletebyid/{empId}")
public int deleteEmployee(@PathVariable int empId) {
	
	return employeeService.deleteEmployeeById(empId);
}
}
