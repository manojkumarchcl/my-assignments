package com.main;

import com.model.Accounts;
import com.model.Customers;


/**
 * @author manoj.kumarc
 *
 */
public class OneToManyMapping {

	public static void main(String[] args) {
		Customers customer = new Customers(76878799, "Manoj");
		Customers customer2 = new Customers(787890, "Vasu");
		Customers[] customersdata = new Customers[2];
		customersdata[0] = customer;
		customersdata[1] = customer2;
		
	Accounts accounts = new Accounts();
	accounts.setAccountNo(677989);
	accounts.setAccountType("Savings");
	accounts.setCustomers(customersdata);
				
		Customers[] customerArray = accounts.getCustomers();
		for (int j = 0; j < customerArray.length; j++) {
			System.out.println("The Customers with multiple Accounts " +customerArray[j].getCustomerName() +"\n");

		}
			
		}
	}

