package com.model;

/**
 * @author manoj.kumarc
 *
 */
public class Accounts {
	private int AccountNo;
	private String AccountType;
	private Customers[] customers;
	public Accounts() {
		super();
	}
	public Accounts(int accountNo, String accountType, Customers[] customer) {
		super();
		AccountNo = accountNo;
		AccountType = accountType;
		this.customers = customer;
	}
	public int getAccountNo() {
		return AccountNo;
	}
	public void setAccountNo(int accountNo) {
		AccountNo = accountNo;
	}
	public String getAccountType() {
		return AccountType;
	}
	public void setAccountType(String accountType) {
		AccountType = accountType;
	}
	public Customers[] getCustomers() {
		return customers;
	}
	public void setCustomers(Customers[] customers) {
		this.customers = customers;
	}
	
	}
