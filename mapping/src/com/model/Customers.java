package com.model;

/**
 * @author manoj.kumarc
 *
 */
public class Customers {
private int customerId;
private String customerName;
public Customers() {
	super();
}
public Customers(int customerId, String customerName) {
	super();
	this.customerId = customerId;
	this.customerName = customerName;
}
public int getCustomerId() {
	return customerId;
}
public void setCustomerId(int customerId) {
	this.customerId = customerId;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}


}

