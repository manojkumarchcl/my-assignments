package com.consumerservice.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.consumerservice.model.Department;
import com.consumerservice.model.EmployeeDto;

@RestController
@RequestMapping(value="Department")
public class ConsumerController {

	@Autowired
	private RestTemplate restTemplate;
	@GetMapping(value="/consume")
public ResponseEntity<String> getFromProducer(){
	HttpHeaders headers = new HttpHeaders();
	headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	HttpEntity<String> entity = new HttpEntity<String>(headers);

	String fromProducer =restTemplate.exchange("http://localhost:9091/employee/hi", HttpMethod.GET, entity, String.class).getBody();
	return new ResponseEntity<String>(fromProducer,HttpStatus.OK);
}
	@PostMapping(value="createDepartment")
	public ResponseEntity<Department> createEmployee(@RequestBody Department department){
		System.out.println("Consumer DeptNo:" +department.getDeptNo());
		System.out.println("Consumer DeptName: " +department.getDeptName());
		System.out.println("Consumer Details: " +department.getEmployeeDtos().get(0).getEmpName());
		return new ResponseEntity<Department>(department, HttpStatus.OK);	
	}
	@GetMapping(value="readDepartment")
	public ResponseEntity<List<Department>> readAllDepartment(){
		List<EmployeeDto> employees1 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/1", ArrayList.class);
		Department department1 = new Department(1, "Development", employees1);
		List<EmployeeDto> employees2 = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/2", ArrayList.class);
		Department department2 = new Department(1, "Development", employees2);
		List<Department> departments = new ArrayList<>();
		departments.add(department1);
		departments.add(department2);
		return new ResponseEntity<List<Department>>(departments, HttpStatus.OK);
		
	}

	@GetMapping(value="readDepartmentByid/{deptId}")
	public ResponseEntity<Department> readDepartmentByDeptId(@PathVariable int deptId){
		List<EmployeeDto> employeeList = restTemplate.getForObject("http://localhost:9091/employee/readEmployee/"+deptId, ArrayList.class);
		Department department1 = new Department(1,"Training", employeeList);
		return new ResponseEntity<Department>(department1, HttpStatus.OK);
	}


}
