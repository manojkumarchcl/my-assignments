package com.consumerservice.model;

import java.util.List;

public class Department {
	private int deptNo;
	private String deptName;
	private List<EmployeeDto> employeeDtos;

	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Department(int deptNo, String deptName, List<EmployeeDto> employeeDtos) {
		super();
		this.deptNo = deptNo;
		this.deptName = deptName;
		this.employeeDtos = employeeDtos;
	}

	public int getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(int deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public List<EmployeeDto> getEmployeeDtos() {
		return employeeDtos;
	}

	public void setEmployeeDtos(List<EmployeeDto> employeeDtos) {
		this.employeeDtos = employeeDtos;
	}
}
