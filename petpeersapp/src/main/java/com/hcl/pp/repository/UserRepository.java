package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.User;
/**
 * @author manoj.kumarc
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
