package com.hcl.pp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.pp.model.Pet;
/**
 * @author manoj.kumarc
 *
 */
@Repository
public interface PetRepository extends JpaRepository<Pet, Integer>{

}
