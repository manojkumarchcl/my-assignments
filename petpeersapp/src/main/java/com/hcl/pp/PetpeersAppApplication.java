package com.hcl.pp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author manoj.kumarc
 *
 */
@SpringBootApplication
@EnableSwagger2
public class PetpeersAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetpeersAppApplication.class, args);
	}

}
