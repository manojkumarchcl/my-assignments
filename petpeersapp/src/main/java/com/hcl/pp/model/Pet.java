package com.hcl.pp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PETS")
public class Pet implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int petId;
	@NotNull
	@Size(min=2, max=30, message ="Please enter the valid name")
	@Column(name = "PET_NAME")
	private String petName;
	@NotNull
	@Min(2)
	@Max(12)
	@Column(name = "PET_AGE")
	private int petAge;
	@NotNull
	@Size(min=2, max=30, message ="Please enter the valid place")
	@Column(name = "PET_PLACE")
	private String petPlace;
	@ManyToOne
	@JoinColumn(name = "PET_OWNERID")
	private User owner;
	}
