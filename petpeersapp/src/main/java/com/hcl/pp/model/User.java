package com.hcl.pp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javassist.SerialVersionUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author manoj.kumarc
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "PET_USER")
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int userId;
	@NotNull
	@Size(min=5, max=9, message="please enter the valid username min 5 characters and maximum 9 characters")
	@Column(name = "USER_NAME")
	private String userName;
	@NotNull
	@Size(min=5, max=12, message="please enter the valid password min 5 characters and maximum 12 characters")
	@Column(name = "USER_PASSWD")
	private String userPassword;
	@JsonIgnoreProperties("owner")
	@OneToMany(mappedBy = "owner")
	private Set<Pet> pets;	
	
}
