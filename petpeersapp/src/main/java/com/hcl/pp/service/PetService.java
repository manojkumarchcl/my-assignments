package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.model.Pet;

/**
 * @author manoj.kumarc
 *
 */
public interface PetService {
	public abstract Pet createPet(Pet pet);

	public abstract Pet readPetById(int petId);

	public abstract Pet updatePet(Pet pet);

	public abstract int deletePetById(int id);

	public abstract List<Pet> readAllPet();
}
