package com.hcl.pp.service;

import java.util.List;

import com.hcl.pp.model.User;

/**
 * @author manoj.kumarc
 *
 */
public interface UserService {
	public abstract User createUser(User user);

	public abstract User readUserById(int userId);

	public abstract User updateUser(User user);

	public abstract int deleteUserById(int id);

	public abstract List<User> readAllUsers();
}
