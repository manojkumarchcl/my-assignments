package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.model.Pet;
import com.hcl.pp.repository.PetRepository;

/**
 * @author manoj.kumarc
 *
 */
@Service
public class PetServiceImpl implements PetService {
	@Autowired
	private PetRepository petRepository;

	@Override
	@Transactional
	public Pet createPet(Pet pet) {
		return petRepository.save(pet);
	}

	@Override
	@Transactional
	public Pet readPetById(int id) {
		Pet pet = null;
		Optional<Pet> optionalPet = petRepository.findById(id);
		if (optionalPet.isPresent()) {
			pet = optionalPet.get();
		}
		return pet;
	}

	@Override
	@Transactional
	public Pet updatePet(Pet pet) {
		return petRepository.save(pet);
	}

	@Override
	@Transactional
	public int deletePetById(int id) {
		petRepository.deleteById(id);
		return id;
	}

	@Override
	@Transactional
	public List<Pet> readAllPet() {
		return petRepository.findAll();
	}

}
