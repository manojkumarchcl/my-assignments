package com.hcl.pp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.pp.model.User;
import com.hcl.pp.repository.UserRepository;
/**
 * @author manoj.kumarc
 *
 */
@Service
public class UserServiceImpl implements UserService {
@Autowired
	private UserRepository userRepository;
	@Override
	@Transactional
	public User createUser(User user) {
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public User readUserById(int userId) {
		User user = null;
		Optional<User> optionalUser = userRepository.findById(userId);
		if (optionalUser.isPresent()) {
			user = optionalUser.get();
		}
		return user;
	}

	@Override
	@Transactional
	public User updateUser(User user) {

		return userRepository.save(user);
	}

	

	@Override
	@Transactional
	public List<User> readAllUsers() {

		return userRepository.findAll();
	}

	@Override
	public int deleteUserById(int id) {
		userRepository.deleteById(id);
		return id;
	}

}
