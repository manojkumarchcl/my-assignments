package com.hcl.pp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.Pet;
import com.hcl.pp.service.PetService;
import com.mysql.cj.log.Log;

import lombok.extern.slf4j.Slf4j;

/**
 * @author manoj.kumarc
 *
 */
@RestController
@Slf4j
@RequestMapping(value = "petmain")
public class PetController {
	@Autowired
	private PetService petService;

	@PostMapping(value = "createpet")
	public ResponseEntity<Pet> createPets(@RequestBody Pet pet) {
		log.info("Created Pet Id is:" +pet.getPetId());
		log.info("Created Pet Name is:" +pet.getPetName());
		log.info("Created Pet Age is:" +pet.getPetAge());
		log.info("Created Pet Place is:" +pet.getPetPlace());
		log.info("Created Pet UserId is:" +pet.getOwner());
		Pet pet1 = null;
		if (pet != null) {
			pet1 = petService.createPet(pet);
		}
		return new ResponseEntity<Pet>(pet1, HttpStatus.CREATED);
	}

	@GetMapping(value = "readallpets")
	public List<Pet> readPets() {
		return petService.readAllPet();
		

	}

	@GetMapping(value = "readbypetid/{petId}")
	public Pet readPetByPetId(@PathVariable int petId) {
		log.info("Read Pet Id is:" +petId);
		Pet pet = null;
		if (petId > 0) {
			pet = petService.readPetById(petId);
			
		}
	
		return pet;
	}

	@PutMapping(value = "/updatepet")
	public Pet alterPet(@RequestBody Pet pet) {
		log.info("Updated Pet Id is:" +pet.getPetId());
		log.info("Updated Pet Name is:" +pet.getPetName());
		log.info("Updated Pet Age is:" +pet.getPetAge());
		log.info("Updated Pet Place is:" +pet.getPetPlace());
		log.info("Updated Pet UserId is:" +pet.getOwner());
		return petService.updatePet(pet);
	}

	@DeleteMapping(value = "deletebypetid/{petId}")
	public int deletepet(@PathVariable int petId) {
		log.info("Deleted Pet Id is:" +petId);
		return petService.deletePetById(petId);
	}
}
