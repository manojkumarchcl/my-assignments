package com.hcl.pp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.pp.model.User;
import com.hcl.pp.service.UserService;
import com.mysql.cj.log.Log;

import lombok.extern.slf4j.Slf4j;

/**
 * @author manoj.kumarc
 *
 */
@RestController
@Slf4j
@RequestMapping(value = "usermain")
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping(value = "createuser")
	public ResponseEntity<User> createUsers(@RequestBody User user) {
		log.info("User Id is:" + user.getUserId());
		log.info("User Name is:" + user.getUserName());
		log.info("User pwd is :" + user.getUserPassword());
		log.info("And the pets list is:" + user.getPets());
		User user1 = null;
		if (user != null) {
			user1 = userService.createUser(user);
		}
		return new ResponseEntity<User>(user1, HttpStatus.CREATED);
	}

	@GetMapping(value = "readbyuserid/{userId}")
	public User readPetByUserId(@PathVariable int userId) {
		log.info("The UserId is: " + userId);
		User user = null;
		if (userId > 0) {
			user = userService.readUserById(userId);
			log.info("And the pets list is: " + user.getPets());
		}
		return user;
	}

	@GetMapping(value = "readallusers")
	public List<User> readUsers() {
		return userService.readAllUsers();

	}

	@PutMapping(value = "/updateuser")
	public User alterUser(@RequestBody User user) {
		log.info("Updated User Id is:" + user.getUserId());
		log.info("Updated User Name is:" + user.getUserName());
		log.info("Updated User pwd is :" + user.getUserPassword());
		log.info("And the pets list is:" + user.getPets());
		return userService.updateUser(user);
	}

	@DeleteMapping(value = "deletebyuserid/{userId}")
	public int deleteUser(@PathVariable int userId) {
		log.info("Deleted The UserId is: " + userId);
		try {
			return userService.deleteUserById(userId);
		} catch (org.springframework.dao.EmptyResultDataAccessException e) {
			e.printStackTrace();
		}
		return userId;
	}

}
