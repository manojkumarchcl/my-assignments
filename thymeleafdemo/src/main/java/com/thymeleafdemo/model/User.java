package com.thymeleafdemo.model;

public class User {

	public String userName;
	public String email;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String userName, String email) {
		super();
		this.userName = userName;
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
