package com.thymeleafdemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thymeleafdemo.model.User;

@Controller
public class UserController {

	@GetMapping(value="/")
	public String first() {
		return "index";//index == user inputs thymeleaf/templates
	}
	
	@PostMapping(value="/save")
	public ModelAndView second(@ModelAttribute User user) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("success");
		modelAndView.addObject("userData", user);
		return modelAndView;
	}
}
