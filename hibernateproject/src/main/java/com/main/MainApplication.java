package com.main;

import java.util.InputMismatchException;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

/**
 * @author manoj.kumarc
 *
 */
public class MainApplication {

	public static void main(String[] args) {

		// StandardServiceRegistry ssr=new
		// StandardServiceRegistryBuilder().configure("com/config/hibernate.cfg.xml").build();
		Scanner scanner = new Scanner(System.in);
		boolean exit = false;
		while (!exit) {
			System.out.println("The options are:" + "\n");
			System.out.println("1. Create an employee Data:" + "\n");
			System.out.println("2. Fetch an employee Data By Employee Id :" + "\n");
			System.out.println("3. Delete an employee Data By Employee Id:" + "\n");
			System.out.println("4. exit" + "\n");
			int option; // We will save the user�s option

			try {
				System.out.println("Select Options:");
				option = scanner.nextInt();
				switch (option) {
				case 1:
					System.out.println("You Have Selected option 1");
					StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
					Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();

					SessionFactory sessionFactory = meta.getSessionFactoryBuilder().build();
					Session session = sessionFactory.openSession();
					// Employee employee = new Employee(687888, "Vasu", 98066.44f);

					Employee employee = new Employee();
					Scanner scanner3 = new Scanner(System.in);
					System.out.println("Enter the Employee Id: ");
					int id = scanner3.nextInt();
					employee.setEmployeeId(id);
					System.out.println("Enter the Employee Name: ");
					String name = scanner3.next();
					employee.setEmployeeName(name);
					System.out.println("Enter the Employee Salary: ");
					float empSalary = scanner3.nextFloat();
					employee.setSalary(empSalary);
					Transaction transaction = session.beginTransaction();
					session.save(employee);
					transaction.commit();
					System.out.println("Employee Id: " + employee.getEmployeeId());
					System.out.println("Employee Name: " + employee.getEmployeeName());
					System.out.println("Employee Salary: " + employee.getSalary());
					System.out.println("Connection Closed" + "\n");

					break;
				case 2:
					System.out.println("\n" + "You have selected option 2");
					StandardServiceRegistry ssr1 = new StandardServiceRegistryBuilder().configure().build();
					Metadata meta1 = new MetadataSources(ssr1).getMetadataBuilder().build();
					SessionFactory sessionFactory1 = meta1.getSessionFactoryBuilder().build();
					Session session2 = sessionFactory1.openSession();
					Employee employee1 = new Employee();
					Scanner scanner4 = new Scanner(System.in);
					System.out.println("Enter the Employee Id:");
					int id1 = scanner4.nextInt();
					employee1.setEmployeeId(id1);
					session2.load(employee1, id1);
					System.out.println("Employee Name is: " + "\n" + employee1.getEmployeeName());
					System.out.println("Employee Salary is: " + "\n" + employee1.getSalary());
					System.out.println("Connection Closed" + "\n");
					break;
				case 3:
					System.out.println("You have selected option 3");
					StandardServiceRegistry ssr2 = new StandardServiceRegistryBuilder().configure().build();
					Metadata meta2 = new MetadataSources(ssr2).getMetadataBuilder().build();
					SessionFactory sessionFactory3 = meta2.getSessionFactoryBuilder().build();
					Session session3 = sessionFactory3.openSession();
					Employee employee2 = new Employee();
					Scanner scanner2 = new Scanner(System.in);
					System.out.println("Enter the Employee Id:");
					int id2 = scanner2.nextInt();
					employee2.setEmployeeId(id2);
					Transaction transaction1 = session3.beginTransaction();
					session3.delete(employee2);
					transaction1.commit();
					System.out.println("\n" + "The employee has been deleted" + "\n");
					System.out.println("Connection Closed" + "\n");
					break;
				case 4:
					System.out.println("You have selected option 4");
					System.out.println("exit");
					StandardServiceRegistry ssr4 = new StandardServiceRegistryBuilder().configure().build();
					Metadata meta4 = new MetadataSources(ssr4).getMetadataBuilder().build();
					SessionFactory sessionFactory4 = meta4.getSessionFactoryBuilder().build();
					Session session4 = sessionFactory4.openSession();
					session4.close();
					System.out.println("Connection Closed" + "\n");

				default:
					System.out.println("Only numbers between 1 to 4");

				}

			} catch (InputMismatchException e) {
				System.out.println("You must insert a number");
				scanner.next();
			}
		}

	}
}
