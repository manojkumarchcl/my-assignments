package com.springgatewayrouting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGatewayRoutingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringGatewayRoutingApplication.class, args);
	}

}
